RAID
====

## layout

(tamaño de las particiones por rhel5 nsa security guide)

soft RAID1: md0 -> /boot (250MB) ext2

soft RAID5: md1 -> LVM group ->
* /swap (4GB)
* /tmp (10GB)
* /var (10GB)
* /var/log (2GB)
* /var/log/audit (1GB)
* / (20GB)
* /data (resto)


if problems with grub, after install, with recovery cd:

select rescue installed system

chroot /mnt/sysimage/

grub-install /dev/md0



## comandos

cat /proc/mdstat 						#info
sudo mdadm --query --detail /dev/md1	#info detallada
hdparm -i /dev/sd% 						#info de los discos
sudo dpkg-reconfigure mdadm 			#reconfigurar (ubuntu)

