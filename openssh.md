openssh
=======

config file: /etc/ssh/sshd_config 
añadido en config:
port: 443
AllowUsers vic # solo victor
PasswordAuthentication no # solo ssh keys

restart sshd:
service sshd restart


http://www.cyberciti.biz/faq/centos-redhat-enterprise-linux6-change-sshd-port-number/ 

By default SELinux only allows port number 22. To display current port contexts, enter:
# semanage port -l | grep ssh

Sample outputs:

ssh_port_t                     tcp      22
To add port 1255 to port contexts, enter:
# semanage port -a -t ssh_port_t -p tcp 1255

You can verify new settings, enter:
# semanage port -l | grep ssh

Sample outputs:
ssh_port_t                     tcp      1255,22

Finally, reload or restart the OpenSSH server, enter:
# /sbin/service sshd reload

Verify, sshd is listing on TCP port # 1255, enter:
# netstat -tulpn | grep 1255
