ip configuration
================


----------------------------------------------------------------
## Configure eth0
#
# vi /etc/sysconfig/network-scripts/ifcfg-eth0
 
DEVICE="eth0"
NM_CONTROLLED="yes"
ONBOOT=yes #enable interface
HWADDR=A4:BA:DB:37:F1:04 # el q sea
TYPE=Ethernet
BOOTPROTO=static
IPADDR=192.168.1.30
NETMASK=255.255.255.0
GATEWAY=192.168.1.1
 
 
## Configure Default Gateway
#
# nano /etc/sysconfig/network
 
NETWORKING=yes
HOSTNAME=centos6
GATEWAY=192.168.1.1
 
 
 
## Configure DNS Server
#
# nano /etc/resolv.conf
 
nameserver 192.168.1.1  # Replace with your nameserver ip



## Restart Network Interface
#
service network restart
